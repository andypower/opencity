<?php /* #?ini charset="utf-8"?

[CookiesSettings]
#simple or advanced
Consent=advanced

[InstanceSettings]
NomeAmministrazioneAfferente=
UrlAmministrazioneAfferente=
LiveIPList[]

[GeneralSettings]
tag_line=
theme=cagliari

[NetworkSettings]
PrototypeUrl=

[CreditsSettings]
Name=OpenCity
Url=http://www.opencontent.it/opencity
Title=OpenCity: dialogare tra enti e cittadini è più facile
CodeUrl=https://gitlab.com/opencontent/opencity
CodeVersion=2.0.0

[ViewSettings]
AvailableView[]=card
AvailableView[]=card_children
AvailableView[]=banner
AvailableView[]=card_teaser
AvailableView[]=card_image
AvailableView[]=banner_color

[Stili]
UsaNeiBlocchi=enabled
Nodo_NomeStile[]
Nodo_NomeStile[]=0;bg-100
Nodo_NomeStile[]=0;section section-muted section-inset-shadow pb-5

[TopMenu]
NodiAreeCustomMenu[]
MaxRecursion=3

[WebsiteToolbar]
ShowMediaRoot=enabled
ShowUsersRoot=disabled
ShowEditorRoles=enabled

[Attributi]
AttributiAbstract[]
AttributiAbstract[]=abstract
AttributiAbstract[]=intro
AttributiAbstract[]=short_description
AttributiAbstract[]=fonte
AttributiAbstract[]=topic
AttributiAbstract[]=tags
AttributiAbstract[]=body
AttributiAbstract[]=edizione
AttributiAbstract[]=text
AttributiAbstract[]=description
AttributiAbstract[]=event_abstract

*/?>
