identifier: homogeneous_organizational_area
contentobject_name: '<legal_name>'
serialized_name_list:
    always-available: ita-IT
    ita-IT: 'Area organizzativa omogenea'
    ger-DE: 'Homogener organisatorischer Bereich (AOO)'
serialized_description_list:
    ita-IT: 'Le Aree Organizzative Omogenee (AOO) identificano gli uffici di protocollo degli Enti che gestiscono i flussi documentali in entrata e in uscita dall''Ente'
    ger-DE: 'Die homogenen Organisationsbereiche (AOO) identifizieren die Protokollbüros der Einrichtugen, die den Dokumentenfluss im Ein- und Ausgang verwalten.'
always_available: 1
sort_field: 9
data_map:
    legal_name:
        identifier: legal_name
        serialized_description_list:
            ita-IT: 'Nome primario o il nome legale dell''organizzazione'
            ger-DE: 'Hauptname oder rechtlicher Name der Organisation'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Nome
            ger-DE: Name
        data_type_string: ezstring
        placement: 1
        is_searchable: 1
        is_required: 1
        can_translate: 1
        serialized_data_text: {  }
    alt_name:
        identifier: alt_name
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'un nome alternativo dell''organizzazione (e.g., un nome colloquiale)'
            ger-DE: 'Alternativer Name der Organisation (z. B. ein umgangssprachlicher Name)'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Nome alternativo'
            ger-DE: 'Alternativer Name'
        data_type_string: ezstring
        placement: 2
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    org_acronym:
        identifier: org_acronym
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'eventuale acronimo dell''organizzazione'
            ger-DE: 'Evt. Kürzel der Organisation'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Acronimo
            ger-DE: Abkürzug
        data_type_string: ezstring
        placement: 3
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    abstract:
        identifier: abstract
        serialized_description_list:
            ita-IT: 'Sintetica descrizione dell''organizzazione'
            ger-DE: 'Synthetische Beschreibung der Organisation'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Descrizione breve'
            ger-DE: 'Kurze Beschreibung'
        data_type_string: ezxmltext
        placement: 4
        can_translate: 1
        data_int1: 5
        serialized_data_text: {  }
    description:
        identifier: description
        serialized_description_list:
            ita-IT: 'Descrizione completa dell''organizzazione'
            ger-DE: 'Detaillierte Beschreibung der Organisation'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Descrizione
            ger-DE: Beschreibung
        data_type_string: ezxmltext
        placement: 5
        can_translate: 1
        data_int1: 10
        serialized_data_text: {  }
    main_function:
        identifier: main_function
        serialized_description_list:
            ita-IT: 'Funzione dell''organizzazione ed elenco dei compiti assegnati'
            ger-DE: 'Funktion der Organisation und Liste der zugewiesenen Aufgaben'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Competenze e funzioni'
            ger-DE: 'Zuständigkeiten und Funktionen'
        data_type_string: ezxmltext
        placement: 6
        can_translate: 1
        data_int1: 5
        serialized_data_text: {  }
    has_logo:
        identifier: has_logo
        serialized_description_list:
            ita-IT: 'Immagine principale o logo dell''organizzazione'
            ger-DE: 'Hauptbild oder Logo der Organisation'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Logo
            ger-DE: Logo
        data_type_string: ezimage
        placement: 7
        can_translate: 1
        serialized_data_text: {  }
    start_time:
        identifier: start_time
        serialized_description_list:
            ita-IT: 'Data a partire dalla quale l''organizzazione è attiva'
            ger-DE: 'Datum, ab dem die Organisation aktiv ist'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data inizio validità'
            ger-DE: 'Gültig/wirksam ab '
        data_type_string: ezdate
        placement: 8
        can_translate: 1
        serialized_data_text: {  }
    end_time:
        identifier: end_time
        serialized_description_list:
            ita-IT: 'Data di fine attività dell''organizzazione'
            ger-DE: 'Datum an dem die Organisation ihre Tätigkeit beendet'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data fine validità'
            ger-DE: 'Gültig/wirksam bis '
        data_type_string: ezdate
        placement: 9
        can_translate: 1
        serialized_data_text: {  }
    has_spatial_coverage:
        identifier: has_spatial_coverage
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Sedi
            ger-DE: Sitz/Adresse
        data_type_string: ezobjectrelationlist
        placement: 10
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - place
            default_placement:
                node_id: $contenttree_Amministrazione_Luoghi_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    accreditation_date:
        identifier: accreditation_date
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'data di accreditamento dell''organizzazione nel pubblico registro IndicePA'
            ger-DE: 'Datum der Akkreditierung der Organisation im öffentlichen Register IndicePA'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data di accreditamento'
            ger-DE: 'Datum der Akkreditierung'
        data_type_string: ezdate
        placement: 11
        is_searchable: 1
        can_translate: 1
        category: meta
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    has_online_contact_point:
        identifier: has_online_contact_point
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Punti di contatto'
            ger-DE: Kontaktstellen
        data_type_string: ezobjectrelationlist
        placement: 12
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - online_contact_point
            default_placement:
                node_id: $contenttree_Classificazioni_Punti-di-contatto_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    foundation_date:
        identifier: foundation_date
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'la data di costituzione dell''organizzazione'
            ger-DE: 'Gründungsdatum der Organisation'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data di costituzione'
            ger-DE: 'Gründungsdatum der Organisation'
        data_type_string: ezdate
        placement: 13
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    has_public_org_activity_type:
        identifier: has_public_org_activity_type
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Tipo di attività'
            ger-DE: Tätigkeitsbereich
        data_type_string: eztags
        placement: 14
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / COFOG)'
        data_int3: 1
        data_int4: 2
        data_text1: Tree
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    hold_employment:
        identifier: hold_employment
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Struttura organizzativa da cui dipende (livello più alto)'
            ger-DE: 'Organisationsstruktur, von der es abhängt (höchste Ebene)'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Legami con altre strutture'
            ger-DE: 'Verbindungen mit anderen Strukturen'
        data_type_string: ezobjectrelationlist
        placement: 15
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - administrative_area
                - office
            default_placement:
                node_id: $contenttree_Amministrazione_Uffici_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    topics:
        identifier: topics
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'tematiche trattate da questa organizzazione'
            ger-DE: 'Themen, die von dieser Organisation behandelt werden'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Argomenti
            ger-DE: Themen
        data_type_string: ezobjectrelationlist
        placement: 16
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '3'
            type: '2'
            class_constraint_list:
                - topic
            default_placement:
                node_id: $contenttree_OpenCity_Argomenti_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    assessore:
        identifier: assessore
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Assessore di riferimento'
            ger-DE: 'Zuständiger Stadtrat'
        data_type_string: ezobjectrelationlist
        placement: 17
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - politico
            default_placement:
                node_id: $contenttree_Amministrazione_Politici_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    allegati:
        identifier: allegati
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Allegati
            ger-DE: Anhänge
        data_type_string: ezobjectrelationlist
        placement: 18
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - document
            default_placement:
                node_id: $contenttree_Documenti-e-dati_Documenti-tecnici-di-supporto_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    more_information:
        identifier: more_information
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Ulteriori informazioni sulla struttura non contemplate dai campi precedenti'
            ger-DE: 'Zusätzliche Informationen über die Struktur, die nicht durch die vorherigen Felder abgedeckt sind'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Ulteriori informazioni'
            ger-DE: 'Weitere Informationen'
        data_type_string: ezxmltext
        placement: 19
        is_searchable: 1
        can_translate: 1
        data_int1: 5
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    help:
        identifier: help
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Eventuali contatti di supporto all''utente. Per esempio i riferimenti dell''URP'
            ger-DE: 'Mögliche Supportkontakte für den Benutzer. Zum Beispiel  Bürgerschalter'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Box d''aiuto'
            ger-DE: Hilfebox
        data_type_string: ezobjectrelationlist
        placement: 20
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - online_contact_point
            default_placement:
                node_id: $contenttree_Classificazioni_Punti-di-contatto_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    identifier:
        identifier: identifier
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Codice stabilito dal Comune, con validità interna'
            ger-DE: 'Von der Stadtverwaltung erstellter Kodex mit interner Gültigkeit'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Identificativo univoco interno'
            ger-DE: 'Einheitliche interne Kennnummer'
        data_type_string: ezstring
        placement: 21
        is_searchable: 1
        can_translate: 1
        category: meta
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    holds_role_in_time:
        identifier: holds_role_in_time
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Riveste un ruolo nel tempo (IN FASE DI REVISIONE - NUOVO DATATYPE)'
            ger-DE: 'Übernimmt im Laufe der Zeit eine Funktion'
        data_type_string: ezobjectrelationlist
        placement: 22
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - time_indexed_role
            default_placement: false
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    tax_code:
        identifier: tax_code
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Codice fiscale'
            ger-DE: Steuernummer
        data_type_string: ezstring
        placement: 23
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    type:
        identifier: type
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Tipo di struttura organizzativa'
            ger-DE: 'Art von Organisationsstruktur'
        data_type_string: eztags
        placement: 24
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / Tipo di struttura organizzativa)'
        data_int3: 1
        data_int4: 1
        data_text1: Tree
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    a_o_o_identifier:
        identifier: a_o_o_identifier
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Questa proprietà rappresenta il codice IPA, il codice univoco, assegnato all''organizzazione pubblica in fase di accreditamento presso l''indice nazionale PA (IPA). Il Codice IPA viene comunicato tramite e-mail dal Gestore dell''IPA al referente dell''Ente.'
            ger-DE: 'Bezieht sich auf den IPA-Kodex,  der dem digitalisierten gesamtstaatlichen Verzeichnis IPA zu entnehmen ist und den öffentlichen Ämtern per E-Mail zugewiesen wird.'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Codice AOO'
            ger-DE: AOO-Kennnummer
        data_type_string: ezstring
        placement: 25
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    public_organization_category:
        identifier: public_organization_category
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Categoria di organizzazione pubblica'
            ger-DE: 'Art der öffentlichen Körperschaft'
        data_type_string: eztags
        placement: 26
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / Tipo di organizzazione pubblica)'
        data_int3: 1
        data_int4: 1
        data_text1: Tree
        category: hidden
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    is_public_organization_of:
        identifier: is_public_organization_of
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'E'' un''organizzazione pubblica appartenente a'
            ger-DE: 'Diese öffentliche Körperschaft gehört zu...'
        data_type_string: ezobjectrelationlist
        placement: 27
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - public_organization
            default_placement:
                node_id: $contenttree_Amministrazione_Enti-e-fondazioni_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    tax_code_e_invoice_service:
        identifier: tax_code_e_invoice_service
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Codice fiscale servizio di fatturazione elettronica'
            ger-DE: 'Steuernummer des elektronischen Verrechnungsdienstes'
        data_type_string: ezstring
        placement: 28
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    legal_status_code:
        identifier: legal_status_code
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'codice della forma giuridica'
            ger-DE: 'Art der Rechtsform'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Forma giuridica'
            ger-DE: Rechtsform
        data_type_string: eztags
        placement: 29
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / Legal status pubblico)'
        data_int3: 1
        data_int4: 1
        data_text1: Tree
        category: hidden
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    participating:
        identifier: participating
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Indicare le organizzazione a cui questa organizzazione partecipa'
            ger-DE: 'Geben Sie die Organisationen an, an der diese Körperschaft beteiligt ist'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Partecipa '
            ger-DE: 'Beteiligungen '
        data_type_string: ezobjectrelationlist
        placement: 30
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - public_organization
            default_placement:
                node_id: $contenttree_Amministrazione_Enti-e-fondazioni_node
        category: hidden
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
groups:
    - 'Organizzazione (COV)'
